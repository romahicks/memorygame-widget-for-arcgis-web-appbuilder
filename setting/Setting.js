// BSD 3-Clause License

// Copyright (c) 2021, Roma Hicks
// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:

// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.

// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.

// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

define([
  'jimu/LayerStructure',
  'dojo/_base/declare',
  'dojo/on',
  'jimu/BaseWidgetSetting',
  'dijit/_WidgetsInTemplateMixin',
  'dijit/form/Select',
  'dijit/form/ValidationTextBox',
  'esri/tasks/query',
  'esri/tasks/QueryTask'
],
function(LayerStructure, declare, on, BaseWidgetSetting, _WidgetsInTemplateMixin, Select, ValidationTextBox, Query, QueryTask) {

  return declare([BaseWidgetSetting, _WidgetsInTemplateMixin], {
    baseClass: 'jimu-widget-memory-training-setting',

    layerOptions: [{label: "--", value: ""}],
    currentLayer: "",
    fieldOptions: [{label: "--", value: ""}],
    currentField: "",

    postCreate: function() {

      this.currentLayer = this.config.layer;
      this.currentField = this.config.field;

      // Get layers from the map.
      var layers = LayerStructure.getInstance().getLayerNodes();
      this.layerOptions = [{label: "--", value: ""}];
      for (const layer of layers) {
        this.layerOptions.push({label: layer.title, value: layer.id});
      }

      on(this.layerSelect, "change", e => {
        this.currentLayer = this.layerSelect.get('value');
        this.getValidLayerData();
      });

      this.getValidLayerData();
    },

    setConfig: function() {
      this.layerSelect.set('options', this.layerOptions);
      this.layerSelect.set('value', this.currentLayer);
      this.fieldSelect.set('options', this.fieldOptions);
      this.fieldSelect.set('value', this.currentField);
    },

    getConfig: function() {
      this.config.layer = this.layerSelect.get('value');
      this.config.field = this.fieldSelect.get('value');
      return this.config;
    },

    // Get the fields found on the selected layer.
    // Also verify that the layerID is valid for the map, in the case of map change.
    getValidLayerData: function() {
      this.fieldOptions = [{label: "--", value: ""}];
      var layerObj = this.map.getLayer(this.currentLayer);
      if (layerObj == undefined || layerObj.url == undefined) {
        this.currentLayer = "";
        this.setConfig();
        return;
      }

      var query = new Query();
      query.outFields = ['*'];
      query.geometry = this.map.extent;
      var task = new QueryTask(layerObj.url);
      task.execute(query, res => {
        for (const field of res.fields) {
          this.fieldOptions.push({label: field.alias, value: field.name});
          this.setConfig();
        }
      }, err => {
        this.currentLayer =  "";
        this.setConfig();
      });
    },

  });
});