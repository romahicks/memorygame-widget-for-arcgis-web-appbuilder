﻿define({
	root:({
		instruction: "Click on the correct geographic feature with the described characteristic.",
		correct: "Correct",
		incorrect: "Incorrect",
		continueString: "Click to continue...",
		inProgress: "In Progress",
		inProgressString: "Click on the correct feature.",
		completed: "Completed",
		completedString: "You have checked all features!",
		score: "Score",
		seen: "Seen",
		scoreSeperator: "/",
		helpWanted: "Translations and bug reports wanted!"
	}) 
});
