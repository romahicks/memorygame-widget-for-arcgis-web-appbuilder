# MemoryTrainer Widget for ArcGIS Web AppBuilder

Simple memory training widget for ArcGIS Web AppBuilder. Display the attribute of a feature and show confirmation if the correct feature is selected on the map. Developed to help emergency services members memorize street names.

Software is licensed with the 'new' BSD 3-clause license.