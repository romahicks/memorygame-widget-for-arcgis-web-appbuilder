// BSD 3-Clause License

// Copyright (c) 2021, Roma Hicks
// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:

// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.

// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.

// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


define([
  'dojo/dom', 
  'dojo/_base/declare', 
//  'dojo/i18n!widgets/MemoryTrainer/nls/strings.js', 
  'dojo/on', 
  'jimu/BaseWidget', 
  'dijit/_WidgetsInTemplateMixin', 
  'esri/tasks/query', 
  'esri/tasks/QueryTask'
],
  function(dom, declare, on, BaseWidget, _WidgetsInTemplateMixin, Query, QueryTask) {
    //To create a widget, you need to derive from BaseWidget.
    return declare([BaseWidget, _WidgetsInTemplateMixin], {
      // Custom widget code goes here

      baseClass: 'jimu-widget-memorytrainer',

      //this property is set by the framework when widget is loaded.
      //name: 'CustomWidget',

      // Basic setup and even hookup.
      startup: function() {
        // Get features and their field value to populate quiz dictionary.
        var layerObj = this.map.getLayer(this.config.layer);
        var query = new Query();
        query.outFields = ["*"];
        query.geometry = this.map.extent;
        var task = new QueryTask(layerObj.url);
        task.execute(query, res => {
          var features = [];
          for (const value of res.features) {
            //this.session.features.push(value.attributes[this.config.field]);
            features.push(value.attributes[this.config.field]);
          }
          this.session.features = features.filter(this.uniqueValues);
          this.session.total = this.session.features.length;
          dom.byId("stat-total").innerText = this.session.total;
          this.selectRandTargetFeature();
        }, err => {
          console.log("***Feature retrieval error:", err);
        });

        // Listen for a user to click a feature.
        on(this.map.infoWindow, "selection-change", e => {
          if (this.session.status == this.gameStatus.PLAY) {
            this.checkSelection();
          }
        });

        // Listen for a user to click the response button.
        on(dom.byId("response-button"), "click", e => {
          if (this.session.status == this.gameStatus.NEXT) {
            this.selectRandTargetFeature();
            this.map.infoWindow.clearFeatures();
            this.map.infoWindow.hide();
            this.updateResponseButton("In Progress", "in-play", "Click on the correct feature.");
          }
        });
      },

      // An enumeration of the game status.
      gameStatus: Object.freeze({
        NEXT: 1,
        PLAY: 2,
        COMPLETED: 3
      }),

      // Structure to store the game status for the current session.
      session: {
        targetFeature: undefined,
        features: [],
        correct: 0,
        tries: 0,
        seen: 0,
        total: 0,
        status: undefined
      },

      // Randomly select a new feature from the features list.
      // Deletes that feature to ensure every feature is seen at least once per session.
      selectRandTargetFeature: function() {
        var pos = Math.floor(Math.random() * this.session.features.length);
        this.session.targetFeature = this.session.features[pos];
        this.session.features.splice(pos, 1);
        dom.byId("target-value").innerText = this.session.targetFeature;
        this.session.seen += 1;
        this.updateStats();
        this.session.status = this.gameStatus.PLAY;
      },

      // Find all unique strings in the array.
      // https://stackoverflow.com/questions/1960473/get-all-unique-values-in-a-javascript-array-remove-duplicates
      uniqueValues: function(value, index, self) {
        return self.indexOf(value) === index;
      },

      // Check the selected features for their correctness.
      checkSelection: function() {
        if (this.map.infoWindow.count > 0) {
          if (this.map.infoWindow.getSelectedFeature().attributes[this.config.field] === this.session.targetFeature) {
            this.updateResponseButton(this.nls.correct, "correct", this.nls.continueString);
            this.session.correct += 1;
          } else {
            this.updateResponseButton(this.nls.incorrect, "incorrect", this.nls.continueString);
          }
          this.session.status = this.gameStatus.NEXT;
          this.session.tries += 1;
          this.updateStats();
          this.isGameCompleted();
        } else {
            this.updateResponseButton(this.nls.inProgress, "in-play", this.nls.inProgressString);
        }
      },

      // Change response button UI upon player's actions.
      // response is a string giving the player a general idea of the game status.
      // prefix is a string prefix of the class and id of the response HTML/CSS.
      // nextAction is a short string telling the player what to do.
      updateResponseButton: function(response, prefix, nextAction) {
            dom.byId("response-status").innerText = response;
            dom.byId("response-button").className = "boxed " + prefix + "-button";
            dom.byId("response-subtext").innerText = nextAction;
      },

      // Check if all features have been seen, if so, change game status to completed.
      isGameCompleted: function() {
        if (this.session.features.length == 0) {
          this.session.status = this.gameStatus.COMPLETE;
          this.updateResponseButton(this.nls.completed, "completed", this.nls.completedString);
          dom.byId("score-row").className = "result-row highlight-row";
        }
      },

      // Update the status of the game stats UI.
      updateStats: function() {
        dom.byId("stat-correct").innerText = this.session.correct;
        dom.byId("stat-tries").innerText = this.session.tries;
        dom.byId("stat-seen").innerText = this.session.seen;
        dom.byId("stat-correct-perc").innerText = (((this.session.correct / this.session.tries) ? (this.session.correct / this.session.tries) : 0) * 100).toFixed(1);
        dom.byId("stat-seen-perc").innerText = ((this.session.seen / this.session.total) * 100).toFixed(1);
      },

    });
  });
